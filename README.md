# WeatherForecast

Demo project demonstrating basic knowledge in Swift programming language:

• consuming weather REST API in JSON format
• Auto Layout and design integration (works on all iPhone devices)
• Cocoa Pods
• Alamofire/AFNetworking 2.0
• git version control
• code style
• refactoring